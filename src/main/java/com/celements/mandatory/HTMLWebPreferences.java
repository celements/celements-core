/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.celements.mandatory;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.annotation.Requirement;
import org.xwiki.context.Execution;
import org.xwiki.model.reference.DocumentReference;

import com.celements.common.classes.IClassCollectionRole;
import com.celements.model.access.IModelAccessFacade;
import com.celements.model.access.exception.DocumentSaveException;
import com.celements.pagetype.PageTypeClasses;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.doc.XWikiDocument;
import com.xpn.xwiki.objects.BaseObject;

@Component("celements.mandatory.htmlwebpreferences")
public class HTMLWebPreferences implements IMandatoryDocumentRole {

  private static final String _SPACE_PREFERENCE_PAGE_TYPE = "SpacePreference";

  private static final Logger LOGGER = LoggerFactory.getLogger(HTMLWebPreferences.class);

  @Requirement("celements.celPageTypeClasses")
  IClassCollectionRole pageTypeClasses;

  @Requirement
  private IModelAccessFacade modelAccess;

  @Requirement
  Execution execution;

  protected XWikiContext getContext() {
    return (XWikiContext) execution.getContext().getProperty("xwikicontext");
  }

  private PageTypeClasses getPageTypeClasses() {
    return (PageTypeClasses) pageTypeClasses;
  }

  @Override
  public List<String> dependsOnMandatoryDocuments() {
    return Collections.emptyList();
  }

  @Override
  public void checkDocuments() throws XWikiException {
    LOGGER.trace("Start checkDocuments in HTMLwebPreferences for [{}]", getContext().getDatabase());
    if (!isSkipCelementsHTMLwebPreferences()) {
      LOGGER.trace("before checkHTMLwebPreferences for [{}]", getContext().getDatabase());
      checkHTMLwebPreferences();
    } else {
      LOGGER.info("skip mandatory checkHTMLwebPreferences for [{}], isSkip [{}]",
          getContext().getDatabase(), isSkipCelementsHTMLwebPreferences());
    }
    LOGGER.trace("end checkDocuments in HTMLwebPreferences for [{}]", getContext().getDatabase());
  }

  boolean isSkipCelementsHTMLwebPreferences() {
    boolean isSkip = getContext().getWiki().ParamAsLong(
        "celements.mandatory.skipHTMLwebPreferences", 0) == 1L;
    LOGGER.trace("skipCelementsHTMLwebPreferences for [{}]: [{}]",
        getContext().getDatabase(), isSkip);
    return isSkip;
  }

  void checkHTMLwebPreferences() throws XWikiException {
    DocumentReference htmlWebPreferencesRef = getHTMLwebPreferencesRef(getContext().getDatabase());
    XWikiDocument wikiPrefDoc = modelAccess.getOrCreateDocument(htmlWebPreferencesRef);
    boolean dirty = checkPageType(wikiPrefDoc);
    dirty |= checkHTMLwebPreferences(wikiPrefDoc);
    if (dirty) {
      try {
        LOGGER.info("HTMLwebPreferencesDocument updated for [{}]", getContext().getDatabase());
        modelAccess.saveDocument(wikiPrefDoc, "autocreate HTML.WebPreferences.");
      } catch (DocumentSaveException dse) {
        throw new XWikiException(0, 0, "failed saving", dse);
      }
    } else {
      LOGGER.debug("HTMLwebPreferencesDocument not saved. Everything uptodate. [{}]",
          getContext().getDatabase());
    }
  }

  boolean checkHTMLwebPreferences(XWikiDocument wikiPrefDoc) throws XWikiException {
    String wikiName = getContext().getDatabase();
    BaseObject prefsObj = wikiPrefDoc.getXObject(getXWikiPreferencesRef(wikiName), false,
        getContext());
    if (prefsObj == null) {
      prefsObj = wikiPrefDoc.newXObject(getXWikiPreferencesRef(wikiName), getContext());
      prefsObj.set("skin", "htmlskin", getContext());
      LOGGER.debug("XWikiPreferences missing fields in wiki preferences object fixed [{}]",
          getContext().getDatabase());
      return true;
    }
    return false;
  }

  boolean checkPageType(XWikiDocument wikiPrefDoc) throws XWikiException {
    DocumentReference pageTypeClassRef = getPageTypeClasses().getPageTypeClassRef(
        getContext().getDatabase());
    BaseObject pageTypeObj = wikiPrefDoc.getXObject(pageTypeClassRef, false, getContext());
    if (pageTypeObj == null) {
      pageTypeObj = wikiPrefDoc.newXObject(pageTypeClassRef, getContext());
      pageTypeObj.setStringValue("page_type", _SPACE_PREFERENCE_PAGE_TYPE);
      LOGGER.debug("HTML.WebPreferences missing page type object fixed for [{}]",
          getContext().getDatabase());
      return true;
    }
    return false;
  }

  private DocumentReference getXWikiPreferencesRef(String wikiName) {
    return new DocumentReference(wikiName, "XWiki", "XWikiPreferences");
  }

  private DocumentReference getHTMLwebPreferencesRef(String wikiName) {
    return new DocumentReference(wikiName, "HTML", "WebPreferences");
  }

}
