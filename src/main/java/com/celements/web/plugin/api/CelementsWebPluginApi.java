/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.celements.web.plugin.api;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.EntityReference;
import org.xwiki.model.reference.SpaceReference;
import org.xwiki.script.service.ScriptService;

import com.celements.appScript.AppScriptScriptService;
import com.celements.auth.AccountActivationFailedException;
import com.celements.auth.IAuthenticationServiceRole;
import com.celements.css.CssScriptService;
import com.celements.emptycheck.service.EmptyCheckScriptService;
import com.celements.javascript.JSScriptService;
import com.celements.mailsender.CelMailScriptService;
import com.celements.navigation.NavigationApi;
import com.celements.navigation.TreeNode;
import com.celements.navigation.service.ITreeNodeService;
import com.celements.navigation.service.TreeNodeCache;
import com.celements.navigation.service.TreeNodeScriptService;
import com.celements.nextfreedoc.NextFreeDocScriptService;
import com.celements.pagetype.IPageType;
import com.celements.pagetype.PageTypeApi;
import com.celements.sajson.Builder;
import com.celements.web.css.CSS;
import com.celements.web.plugin.CelementsWebPlugin;
import com.celements.web.plugin.cmd.CaptchaCommand;
import com.celements.web.plugin.cmd.ISynCustom;
import com.celements.web.plugin.cmd.PageLayoutCommand;
import com.celements.web.service.CelementsWebScriptService;
import com.celements.web.service.ContextMenuScriptService;
import com.celements.web.service.EditorSupportScriptService;
import com.celements.web.service.IWebUtilsService;
import com.celements.web.service.WebUtilsScriptService;
import com.celements.web.service.WebUtilsService;
import com.celements.web.utils.SuggestBaseClass;
import com.celements.webform.ActionScriptService;
import com.celements.webform.WebFormScriptService;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.api.Api;
import com.xpn.xwiki.api.Attachment;
import com.xpn.xwiki.api.Document;
import com.xpn.xwiki.user.api.XWikiUser;
import com.xpn.xwiki.web.Utils;

/**
 * @Deprecated: since 2.59 instead use class {@link CelementsWebScriptService} or a
 *              special named ScriptService
 */
@Deprecated
public class CelementsWebPluginApi extends Api {

  /**
   * @Deprecated: since 2.59 instead use variable in {@link CssScriptService}
   */
  @Deprecated
  public static final String CELEMENTS_CSSCOMMAND = CssScriptService.CELEMENTS_CSSCOMMAND;

  /**
   * @Deprecated: since 2.59 instead use variable in {@link JSScriptService}
   */
  @Deprecated
  public static final String JAVA_SCRIPT_FILES_COMMAND_KEY = JSScriptService.JAVA_SCRIPT_FILES_COMMAND_KEY;

  private static final Logger LOGGER = LoggerFactory.getLogger(CelementsWebPluginApi.class);

  private CelementsWebPlugin plugin;

  /**
   * @deprecated since 2.59
   */
  @Deprecated
  public CelementsWebPluginApi(CelementsWebPlugin plugin, XWikiContext context) {
    super(context);
    setPlugin(plugin);
  }

  /**
   * @deprecated since 2.59
   */
  @Deprecated
  // FIXME must not be public! why do we need it in the API class anyway?
  public void setPlugin(CelementsWebPlugin plugin) {
    this.plugin = plugin;
  }

  /**
   * @deprecated since 2.59 instead use {@link TreeNodeCache} Do not call flushCache for
   *             MenuItem changes anymore. The TreeNodeDocument change listener take care
   *             of flushing the cache if needed.
   */
  @Deprecated
  public void flushCache() {
    LOGGER.warn("flushCache called. Do not call flushCache for MenuItem "
        + " changes anymore. The TreeNodeDocument change listener take care of flushing "
        + " the cache if needed.");
  }

  /**
   * @deprecated since 2.2 instead use {@link TreeNodeScriptService #createNavigation()}
   */
  @Deprecated
  public NavigationApi createNavigation() {
    return getTreeNodeScriptService().createNavigation();
  }

  /**
   * @deprecated since 2.2 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParentRef(EntityReference)}
   */
  @Deprecated
  public List<com.xpn.xwiki.api.Object> getSubMenuItemsForParent(String parent, String menuSpace) {
    return plugin.getSubMenuItemsForParent(parent, menuSpace, "", context);
  }

  /**
   * @deprecated since 2.2 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParentRef(EntityReference)}
   */
  @Deprecated
  public List<com.xpn.xwiki.api.Object> getSubMenuItemsForParent(String parent, String menuSpace,
      String menuPart) {
    return plugin.getSubMenuItemsForParent(parent, menuSpace, menuPart, context);
  }

  /**
   * @deprecated since 2.24.0 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParentRef(EntityReference)}
   */
  @Deprecated
  public List<TreeNode> getSubNodesForParent(String parent, String menuSpace) {
    return getTreeNodeService().getSubNodesForParent(parent, menuSpace, "");
  }

  /**
   * @deprecated since 2.2 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParentRef(EntityReference)}
   */
  @Deprecated
  public List<TreeNode> getSubNodesForParentRef(EntityReference parentRef) {
    return getTreeNodeScriptService().getSubNodesForParent(parentRef, "");
  }

  /**
   * @deprecated since 2.24.0 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParentRef(EntityReference)}
   */
  @Deprecated
  public List<TreeNode> getSubNodesForParent(String parent, String menuSpace, String menuPart) {
    return getTreeNodeService().getSubNodesForParent(parent, menuSpace, menuPart);
  }

  /**
   * @deprecated since 2.2 instead use
   *             {@link TreeNodeScriptService #getSubNodesForParent(EntityReference, String)}
   */
  @Deprecated
  public List<TreeNode> getSubNodesForParent(EntityReference parentRef, String menuPart) {
    return getTreeNodeScriptService().getSubNodesForParent(parentRef, menuPart);
  }

  /**
   * @deprecated since 2.33.0 instead use
   *             {@link CelementsWebScriptService #getDocMetaTags(String, String)}
   */
  @Deprecated
  public Map<String, String> getDocMetaTags(String language, String defaultLanguage) {
    return getScriptService().getDocMetaTags(language, defaultLanguage);
  }

  /**
   * @deprecated since 2.33.0 instead use
   *             {@link WebUtilsService #getAttachmentListSorted(Document, String)}
   */
  @Deprecated
  public List<Attachment> getAttachmentListForTagSortedSpace(String spaceName, String tagName,
      String comparator, boolean imagesOnly, int start, int nb) throws ClassNotFoundException {
    return getWebUtilsService().getAttachmentListForTagSortedSpace(spaceName, tagName, comparator,
        imagesOnly, start, nb);
  }

  public List<Attachment> getAttachmentListSorted(Document doc, String comparator)
      throws ClassNotFoundException {
    return getWebUtilsScriptService().getAttachmentListSorted(doc, comparator);
  }

  /**
   * @deprecated since 2.33.0 instead use
   *             {@link WebUtilsService #getAttachmentListSorted(Document, String, boolean, int,
   *             int)}
   */
  @Deprecated
  public List<Attachment> getAttachmentListSorted(Document doc, String comparator,
      boolean imagesOnly, int start, int nb) throws ClassNotFoundException {
    return getWebUtilsScriptService().getAttachmentListSorted(doc, comparator, imagesOnly, start,
        nb);
  }

  public List<Attachment> getAttachmentListForTagSorted(Document doc, String tagName,
      String comparator, boolean imagesOnly, int start, int nb) {
    return getWebUtilsService().getAttachmentListForTagSorted(doc, tagName, comparator, imagesOnly,
        start, nb);
  }

  /**
   * @deprecated since 2.11.3 instead use
   *             {@link CelementsWebScriptService #addImageMapConfig(String)}
   */
  @Deprecated
  public void addImageMapConfig(String configName) {
    getScriptService().addImageMapConfig(configName);
  }

  /**
   * @deprecated since 2.59 instead use {@link JSScriptService #addExtJSfileOnce(String)}
   */
  @Deprecated
  public String addExtJSfileOnce(String jsFile) {
    return getJSScriptService().addExtJSfileOnce(jsFile);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link JSScriptService #addExtJSfileOnce(String, String)}
   */
  @Deprecated
  public String addExtJSfileOnce(String jsFile, String action) {
    return getJSScriptService().addExtJSfileOnce(jsFile, action);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #getNextObjPageId(SpaceReference,
   *             DocumentReference, String)}
   */
  @Deprecated
  public int getNextObjPageId(String spacename, String classname, String propertyName)
      throws XWikiException {
    SpaceReference spaceRef = getWebUtilsService().resolveSpaceReference(spacename);
    if (spaceRef != null) {
      return getScriptService().getNextObjPageId(spaceRef,
          getWebUtilsService().resolveDocumentReference(classname), propertyName);
    }
    return 1;
  }

  // /**
  // * If a template in the template dir on disk is parsed the hasProbrammingRights will
  // * return false, because the sdoc and idoc are NULL. hasCelProgrammingRights in
  // contrast
  // * returns true in this case.
  // * TODO: Check if there are other cases in which the sdoc AND idoc are null. Check
  // * TODO: if there is a better way to recognize that a template from disk is rendered.
  // *
  // * @return
  // */
  // public boolean hasCelProgrammingRights() {
  // return (hasProgrammingRights()
  // || ((context.get("sdoc") == null) && (context.get("idoc") == null)));
  // }

  /**
   * @deprecated since 2.59 instead use {@link CssScriptService #getRTEContentCSS()}
   */
  @Deprecated
  public List<CSS> getRTEContentCSS() throws XWikiException {
    return getCSSScriptService().getRTEContentCSS();
  }

  /**
   * @deprecated since 2.59 instead use {@link CssScriptService #includeCSSPage(String)}
   */
  @Deprecated
  public void includeCSSPage(String css) {
    getCSSScriptService().includeCSSPage(css);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CssScriptService #includeCSSAfterPreferences(String)}
   */
  @Deprecated
  public void includeCSSAfterPreferences(String css) throws XWikiException {
    getCSSScriptService().includeCSSAfterPreferences(css);
  }

  /**
   * @deprecated since 2.9.4 instead use
   *             {@link EmptyCheckScriptService #isEmptyRTEDocument(DocumentReference)}
   **/
  @Deprecated
  public boolean isEmptyRTEDocument(String fullName) {
    return getEmptyCheckScriptService().isEmptyRTEDocument(
        getWebUtilsService().resolveDocumentReference(fullName));
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link EmptyCheckScriptService #isEmptyRTEDocument(DocumentReference)}
   */
  @Deprecated
  public boolean isEmptyRTEDocument(DocumentReference documentRef) {
    return getEmptyCheckScriptService().isEmptyRTEDocument(documentRef);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #getEmailAdressForCurrentUser()}
   */
  @Deprecated
  public String getEmailAdressForCurrentUser() {
    return getScriptService().getEmailAdressForCurrentUser();
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link AuthenticationScriptService #activateAccount(String)}
   */
  @Deprecated
  public Map<String, String> activateAccount(String activationCode) {
    try {
      return getAuthenticationService().activateAccount(activationCode);
    } catch (AccountActivationFailedException authExp) {
      LOGGER.info("Failed to activate account", authExp);
    }
    return Collections.emptyMap();
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link WebUtilsScriptService #getDocumentParentsDocRefList(DocumentReference,
   *             boolean)}
   *             Returns a list of all parent for a specified doc
   * @param fullName
   * @param includeDoc
   * @return List of all parents, starting at the specified doc (bottom up)
   */
  @Deprecated
  public List<DocumentReference> getDocumentParentsDocRefList(DocumentReference docRef,
      boolean includeDoc) {
    return getWebUtilsScriptService().getDocumentParentsDocRefList(docRef, includeDoc);
  }

  /**
   * provides a pageTypeApi for the celements document <code>fullname</code>. e.g.
   * getPageType(fullName).getPageType() provides the PageType name given by the
   * pageType-object on the fullname.
   *
   * @param fullName
   *          of the celements document
   * @return
   * @throws XWikiException
   * @deprecated since 2.21.0 instead use
   *             com.celements.pagetype.service.PageTypeScriptService.getPageTypeConfig()
   */
  @Deprecated
  public IPageType getPageType(String fullName) throws XWikiException {
    return new PageTypeApi(fullName, context);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelMailScriptService #sendMail(String, String, String, String, String,
   *             String, String, String, List, Map)}
   */
  @Deprecated
  public int sendMail(String from, String replyTo, String to, String cc, String bcc, String subject,
      String htmlContent, String textContent, List<Attachment> attachments,
      Map<String, String> others) {
    return getCelMailScriptService().sendMail(from, replyTo, to, cc, bcc, subject, htmlContent,
        textContent, attachments, others);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link WebUtilsScriptService #getAttachmentsForDocs(List)}
   */
  @Deprecated
  public List<Attachment> getAttachmentsForDocs(List<String> docsFN) {
    return getWebUtilsScriptService().getAttachmentsForDocs(docsFN);
  }

  /**
   * @deprecated since 2.59
   */
  @Deprecated
  public int sendLatin1Mail(String from, String replyTo, String to, String cc, String bcc,
      String subject, String htmlContent, String textContent, List<Attachment> attachments,
      Map<String, String> others) {
    DocumentReference docRef = context.getDoc().getDocumentReference();
    LOGGER.warn("usage of deprecated sendLatin1Mail on [" + getWebUtilsService().serializeRef(
        docRef) + "].");
    return plugin.sendMail(from, replyTo, to, cc, bcc, subject, htmlContent, textContent,
        attachments, others, true, context);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link NextFreeDocScriptService #getNextTitledPageDocRef(String, String)}
   */
  @Deprecated
  public DocumentReference getNextTitledPageDocRef(String space, String title) {
    return getNextFreeDocScriptService().getNextTitledPageDocRef(space, title);
  }

  /**
   * @deprecated since 2.30.0 instead use
   *             {@link NextFreeDocScriptService #getNextTitledPageDocRef(String, String)}
   */
  @Deprecated
  public String getNextTitledPageFullName(String space, String title) {
    return getWebUtilsService().getRefLocalSerializer().serialize(getNextTitledPageDocRef(space,
        title));
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link NextFreeDocScriptService #getNextUntitledPageFullName(String)}
   */
  @Deprecated
  public String getNextUntitledPageFullName(String space) {
    return getNextFreeDocScriptService().getNextUntitledPageFullName(space);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link NextFreeDocScriptService #getNextUntitledPageName(String)}
   */
  @Deprecated
  public String getNextUntitledPageName(String space) {
    return getNextFreeDocScriptService().getNextUntitledPageName(space);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link WebUtilsScriptService #getAllowedLanguages()}
   */
  @Deprecated
  public List<String> getAllowedLanguages() {
    return getWebUtilsScriptService().getAllowedLanguages();
  }

  /**
   * @deprecated since 2.59 instead use {@link CelementsWebScriptService #createUser()}
   */
  @Deprecated
  public int createUser() throws XWikiException {
    return getScriptService().createUser(true);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #createUser(boolean)}
   */
  @Deprecated
  public int createUser(boolean validate) throws XWikiException {
    return getScriptService().createUser(validate);
  }

  @Deprecated
  private ISynCustom getSynCustom() {
    return (ISynCustom) Utils.getComponent(ScriptService.class, "syncustom");
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link AuthenticationScriptService #checkAuth(String, String, String, String)}
   *             Check authentication from logincredential and password and set according
   *             persitent login information If it fails user is unlogged
   * @param username
   *          logincredential to check
   * @param password
   *          password to check
   * @param rememberme
   *          "1" if you want to remember the login accross navigator restart
   * @return null if failed, non null XWikiUser if sucess
   * @throws XWikiException
   */
  @Deprecated
  public XWikiUser checkAuth(String logincredential, String password, String rememberme,
      String possibleLogins) throws XWikiException {
    return getAuthenticationService().checkAuth(logincredential, password, rememberme,
        possibleLogins, null);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link AuthenticationScriptService #checkAuth(String, String, String, String,
   *             boolean)}
   *             Check authentication from logincredential and password and set according
   *             persitent login information If it fails user is unlogged
   * @param username
   *          logincredential to check
   * @param password
   *          password to check
   * @param rememberme
   *          "1" if you want to remember the login accross navigator restart
   * @param noRedirect
   *          supress auto redirect to xredirect parameter
   * @return null if failed, non null XWikiUser if sucess
   * @throws XWikiException
   */
  @Deprecated
  public XWikiUser checkAuth(String logincredential, String password, String rememberme,
      String possibleLogins, boolean noRedirect) throws XWikiException {
    return getAuthenticationService().checkAuth(logincredential, password, rememberme,
        possibleLogins, noRedirect);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #addTranslation(DocumentReference, String)}
   */
  @Deprecated
  public boolean addTranslation(DocumentReference docRef, String language) {
    return getScriptService().addTranslation(docRef, language);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #renameSpace(String, String)}
   */
  @Deprecated
  public List<String> renameSpace(String spaceName, String newSpaceName) {
    return getScriptService().renameSpace(spaceName, newSpaceName);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #renameDoc(String, String)}
   */
  @Deprecated
  public boolean renameDoc(String fullName, String newDocName) {
    return getScriptService().renameDoc(getWebUtilsService().resolveDocumentReference(fullName),
        newDocName);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #getSupportedAdminLanguages()}
   */
  @Deprecated
  public List<String> getSupportedAdminLanguages() {
    return getScriptService().getSupportedAdminLanguages();
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link LayoutScriptService #getActivePageLayouts()}
   */
  @Deprecated
  public Map<String, String> getActivePageLayouts() {
    return new PageLayoutCommand().getActivePageLyouts();
  }

  /**
   * @deprecated since 2.59 instead use {@link LayoutScriptService #getAllPageLayouts()}
   */
  @Deprecated
  public Map<String, String> getAllPageLayouts() {
    return new PageLayoutCommand().getAllPageLayouts();
  }

  /**
   * @deprecated since 2.59 instead use {@link WebFormScriptService #isFormFilled(String)}
   */
  @Deprecated
  public boolean isFormFilled(String excludeFields) {
    return getWebFormScriptService().isFormFilled(excludeFields);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #resetProgrammingRights()}
   */
  @Deprecated
  public boolean resetProgrammingRights() {
    return getScriptService().resetProgrammingRights();
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link TreeNodeScriptService #navReorderSave(String, String)}
   */
  @Deprecated
  public String navReorderSave(String fullName, String structureJSON) {
    return getTreeNodeScriptService().navReorderSave(getWebUtilsService().resolveDocumentReference(
        fullName), structureJSON);
  }

  /**
   * renderCelementsDocument
   *
   * @param elementFullName
   * @return
   * @deprecated since 2.11.2 use
   *             {@link CelementsWebScriptService #renderCelementsDocument(DocumentReference)}
   *             instead
   */
  @Deprecated
  public String renderCelementsDocument(String elementFullName) {
    return renderCelementsDocument(elementFullName, "view");
  }

  /**
   * @param elementFullName
   * @param renderMode
   * @return
   * @deprecated since 2.11.2 use
   *             {@link CelementsWebScriptService #renderCelementsDocument(DocumentReference,
   *             String)}
   */
  @Deprecated
  public String renderCelementsDocument(String elementFullName, String renderMode) {
    return getScriptService().renderCelementsDocument(getWebUtilsService().resolveDocumentReference(
        elementFullName), renderMode);
  }

  /**
   * @deprecated since 2.11.7 instead use
   *             {@link CelementsWebScriptService #renderCelementsDocument(DocumentReference)}
   */
  @Deprecated
  public String renderCelementsDocument(DocumentReference elementDocRef) {
    return getScriptService().renderCelementsDocument(elementDocRef);
  }

  /**
   * @deprecated since 2.11.7 instead use
   *             {@link CelementsWebScriptService #renderCelementsDocument(DocumentReference,
   *             String)}
   */
  @Deprecated
  public String renderCelementsDocument(DocumentReference elementDocRef, String renderMode) {
    return getScriptService().renderCelementsDocument(elementDocRef, renderMode);
  }

  /**
   * @deprecated since 2.17.0 instead use
   *             {@link CelementsWebScriptService#renderDocument(Document)}
   */
  @Deprecated
  public String renderDocument(Document renderDoc) {
    return getScriptService().renderDocument(renderDoc);
  }

  /**
   * @deprecated since 2.17.0 instead use
   *             {@link CelementsWebScriptService #renderDocument(Document, boolean, List)}
   */
  @Deprecated
  public String renderDocument(Document renderDoc, boolean removePre,
      List<String> rendererNameList) {
    return getScriptService().renderDocument(renderDoc, removePre, rendererNameList);
  }

  /**
   * @deprecated since 2.17.0 instead use
   *             {@link CelementsWebScriptService #renderCelementsDocument(Document)}
   */
  @Deprecated
  public String renderCelementsDocument(Document renderDoc) {
    return renderCelementsDocument(renderDoc, "view");
  }

  /**
   * @deprecated since 2.17.0 instead use
   *             {@link CelementsWebScriptService #renderCelementsDocument(Document, String)}
   */
  @Deprecated
  public String renderCelementsDocument(Document renderDoc, String renderMode) {
    return getScriptService().renderCelementsDocument(renderDoc, renderMode);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #getEditURL(Document)}
   */
  @Deprecated
  public String getEditURL(Document doc) {
    return getScriptService().getEditURL(doc);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #clearFileName(String)}
   */
  @Deprecated
  public String clearFileName(String fileName) {
    return getScriptService().clearFileName(fileName);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #getDocHeaderTitle(DocumentReference)}
   */
  @Deprecated
  public String getDocHeaderTitle(DocumentReference docRef) {
    return getScriptService().getDocHeaderTitle(docRef);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #newObjectForFormStorage(Document, String)}
   */
  @Deprecated
  public com.xpn.xwiki.api.Object newObjectForFormStorage(Document storageDoc, String className) {
    return getScriptService().newObjectForFormStorage(storageDoc, className);
  }

  /**
   * @deprecated since 2.59 instead use {@link CaptchaScriptService #checkCaptcha()}
   */
  @Deprecated
  public boolean checkCaptcha() {
    return new CaptchaCommand().checkCaptcha(context);
  }

  /**
   * @deprecated since 2.59 instead use {@link CaptchaScriptService #getCaptchaId()}
   */
  @Deprecated
  public String getCaptchaId() {
    return new CaptchaCommand().getCaptchaId(context);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link CelementsWebScriptService #isCelementsRights(String)}
   */
  @Deprecated
  public boolean isCelementsRights(String fullName) {
    return getScriptService().isCelementsRights(getWebUtilsService().resolveDocumentReference(
        fullName));
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link ActionScriptService #executeAction(Document)}
   */
  @Deprecated
  public boolean executeAction(Document actionDoc) {
    return getActionScriptService().executeAction(actionDoc);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link ActionScriptService #executeAction(Document, Map)}
   */
  @Deprecated
  public boolean executeAction(Document actionDoc, Map<String, List<Object>> fakeRequestMap) {
    return getActionScriptService().executeAction(actionDoc, fakeRequestMap);
  }

  /**
   * API to check rights on a document for a given user or group
   *
   * @deprecated since 2.59 instead use
   *             {@link AuthenticationScriptService #hasAccessLevel(String, String, boolean,
   *             String)}
   * @param level
   *          right to check (view, edit, comment, delete)
   * @param user
   *          user or group for which to check the right
   * @param isUser
   *          true for users and false for group
   * @param docname
   *          document on which to check the rights
   * @return true if right is granted/false if not
   */
  @Deprecated
  public boolean hasAccessLevel(String level, String user, boolean isUser, String docname) {

    try {
      return getAuthenticationService().hasAccessLevel(level, user, isUser,
          getWebUtilsService().resolveDocumentReference(docname));
    } catch (Exception exp) {
      LOGGER.warn("hasAccessLevel failed for level[" + level + "] user[" + user + "] " + "docname["
          + docname + "] isUser[" + isUser + "]", exp);
      return false;
    }
  }

  /**
   * @deprecated since 2.11.6 instead use celementsweb script service
   */
  @Deprecated
  public String getSkinFile(String fileName) {
    return getScriptService().getSkinFile(fileName);
  }

  /**
   * @deprecated since 2.11.6 instead use celementsweb script service
   */
  @Deprecated
  public String getSkinFile(String fileName, String action) {
    return getScriptService().getSkinFile(fileName, action);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link EditorSupportScriptService #getSuggestBaseClass(DocumentReference, String)}
   */
  @Deprecated
  public SuggestBaseClass getSuggestBaseClass(DocumentReference classreference, String fieldname) {
    return getEditorSupportScriptService().getSuggestBaseClass(classreference, fieldname);
  }

  /**
   * @deprecated since 2.59 instead use
   *             {@link EditorSupportScriptService #getSuggestList(DocumentReference, String,
   *             String)}
   */
  @Deprecated
  public List<Object> getSuggestList(DocumentReference classRef, String fieldname, String input) {
    return getEditorSupportScriptService().getSuggestList(classRef, fieldname, input);
  }

  /**
   * @deprecated since 2.59 instead use {@link EditorSupportScriptService
   *             #getSuggestList(DocumentReference, String, List, String, String, String,
   *             int))}
   */
  @Deprecated
  public List<Object> getSuggestList(DocumentReference classRef, String fieldname,
      List<String> excludes, String input, String firstCol, String secCol, int limit) {
    return getEditorSupportScriptService().getSuggestList(classRef, fieldname, excludes, input,
        firstCol, secCol, limit);
  }

  /**
   * @deprecated since 2.11.2 instead use {@link ImageScriptService #useImageAnimations()}
   *             in the celements-photo-component Component
   */
  @Deprecated
  public boolean useImageAnimations() {
    String defaultValue = context.getWiki().Param("celements.celImageAnimation", "0");
    return "1".equals(context.getWiki().getSpacePreference("celImageAnimation", defaultValue,
        context));
  }

  /**
   * @deprecated since 2.11.2 instead use celementsweb script service
   */
  @Deprecated
  public Builder getNewJSONBuilder() {
    return getScriptService().getNewJSONBuilder();
  }

  private CelementsWebScriptService getScriptService() {
    return (CelementsWebScriptService) Utils.getComponent(ScriptService.class, "celementsweb");
  }

  private WebUtilsScriptService getWebUtilsScriptService() {
    return (WebUtilsScriptService) Utils.getComponent(ScriptService.class, "webUtils");
  }

  private ContextMenuScriptService getContextMenuScriptService() {
    return (ContextMenuScriptService) Utils.getComponent(ScriptService.class, "contextMenu");
  }

  private TreeNodeScriptService getTreeNodeScriptService() {
    return (TreeNodeScriptService) Utils.getComponent(ScriptService.class, "treeNode");
  }

  private ITreeNodeService getTreeNodeService() {
    return Utils.getComponent(ITreeNodeService.class);
  }

  private IWebUtilsService getWebUtilsService() {
    return Utils.getComponent(IWebUtilsService.class);
  }

  private IAuthenticationServiceRole getAuthenticationService() {
    return Utils.getComponent(IAuthenticationServiceRole.class);
  }

  private EditorSupportScriptService getEditorSupportScriptService() {
    return (EditorSupportScriptService) Utils.getComponent(ScriptService.class, "editorsupport");
  }

  private ActionScriptService getActionScriptService() {
    return (ActionScriptService) Utils.getComponent(ScriptService.class, "action");
  }

  private CssScriptService getCSSScriptService() {
    return (CssScriptService) Utils.getComponent(ScriptService.class, "css");
  }

  private NextFreeDocScriptService getNextFreeDocScriptService() {
    return (NextFreeDocScriptService) Utils.getComponent(ScriptService.class, "nextfreedoc");
  }

  private CelMailScriptService getCelMailScriptService() {
    return (CelMailScriptService) Utils.getComponent(ScriptService.class, "celmail");
  }

  private JSScriptService getJSScriptService() {
    return (JSScriptService) Utils.getComponent(ScriptService.class, "javascript");
  }

  private AppScriptScriptService getAppScriptScriptService() {
    return (AppScriptScriptService) Utils.getComponent(ScriptService.class, "appscript");
  }

  private WebFormScriptService getWebFormScriptService() {
    return (WebFormScriptService) Utils.getComponent(ScriptService.class, "webform");
  }

  private EmptyCheckScriptService getEmptyCheckScriptService() {
    return (EmptyCheckScriptService) Utils.getComponent(ScriptService.class, "emptycheck");
  }

}
